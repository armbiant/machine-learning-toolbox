clear
clc

pyImport numpy
np = numpy
pyImport sklearn.metrics.pairwise
pyImport sklearn.kernel_ridge
kr = sklearn.kernel_ridge

n_samples = 10
n_features = 5
py = pyBuiltin()

rng = np.random.RandomState(py.int(0))

function [pred] = predict(classifier, X)

    k = classifier._get_kernel(classifier,X, classifier.X_fit_)
    pred = k * classifier.dual_coef_

endfunction


y = rng.randn(n_samples)
X = rng.randn(n_samples, n_features)

classifier = kr.KernelRidge(alpha=1.0)
classifier.fit(X, y)

dual_coef = classifier.dual_coef_
X_fit = classifier.X_fit_


y_pred = predict(classifier, X))



