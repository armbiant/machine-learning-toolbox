// Importing necessary python libraries
pyImport pickle
py = pyBuiltin()
pyImport numpy


// using PIMS to run a local ml python script using pyExecFile
pyExecFile("linear_regression.py")


// Waiting for training to complete and the "attributes" pickle file being created
sleep(3000);


// loading the learned attributes in pickle file to Scilab console 
abc = pickle.load(py.open("attributes.p","rb"))


// Using index addressing to extract individual attributes
// To be modified according to the ml model used

// Currently written for linear regression
y_pred = abc(0)'
coef_ = abc(1)
intercept_ = abc(2)
